# Free Culture

Repositorio de ebooks con licencias Creative Commons

Esta colección de libros digitales fue creada con la esperanza de cambiar el mundo. Es el resultado de años de recopilación, y ahora es nuestro. Hacé buen uso del mismo: dedicale las horas de lectura que se merece, contactá a los autores, reseñá el contenido que más te haya influenciado y, si está dentro de tus posibilidades, doná dinero a les autores y/o comprá sus libros. Si bien los contenidos están registrados bajo licencias Creative Commons, Kopimi, o directamente sin lincencia alguna (y por ende es legal compartirlos gratuitamente), el mundo en que vivimos nos empuja a hacer circular el dinero de un lado al otro. Hacéselo llegar a las personas que crean cosas para ayudarnos a crecer y a tener una vida más saludable. Por último, asociate. Acudí a encuentros donde se divulgue la cultura libre, escribí software libre, publicá tu propio material y compartilo con tanta gente como sea posible... El dinero no debe ser un impedimento para sacarle posibilidades a algunos y dárselas a otros. Hacé tu parte. No te canses de compartir.

Con amor,

					@CutSomeSlack
